package fr.ulille.iut.pizzaland.beans;

import fr.ulille.iut.pizzaland.dto.CommandeCreateDto;
import fr.ulille.iut.pizzaland.dto.CommandeDto;

public class Commande {
	private long id;
	private String prenom;
	private String nom;
	
	public Commande() {
	}

	public Commande(long id, String name) {
		this.id = id;
		this.nom = name;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public String getFirstName() {
		return prenom;
	}

	public void setFirstName(String name) {
		this.prenom = name;
	}
	
	public String getLastName() {
		return nom;
	}

	public void setLastName(String name) {
		this.nom = name;
	}
	
	public static CommandeDto toDto(Commande c) {
		CommandeDto dto = new CommandeDto();
		dto.setId(c.getId());
		dto.setFirstName(c.getFirstName());
		dto.setLastName(c.getLastName());
		return dto;
	}

	public static Commande fromDto(CommandeDto dto) {
		Commande Commande = new Commande();
		Commande.setId(dto.getId());
		Commande.setFirstName(dto.getFirstName());
		Commande.setLastName(dto.getLastName());

		return Commande;
	}
	
	public static CommandeCreateDto toCreateDto(Commande Commande) {
	    CommandeCreateDto dto = new CommandeCreateDto();
	    dto.setFirstName(Commande.getFirstName());
	    dto.setLastName(Commande.getLastName());
	    
	    return dto;
	}

	public static Commande fromCommandeCreateDto(CommandeCreateDto dto) {
	    Commande Commande = new Commande();
	    Commande.setFirstName(dto.getFirstName());
	    Commande.setLastName(dto.getLastName());

	    return Commande;
	}
	
	@Override
	public String toString() {
		return prenom + " " + nom;
	}
}
