package fr.ulille.iut.pizzaland;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.dao.CommandeDao;
import fr.ulille.iut.pizzaland.dto.CommandeCreateDto;
import fr.ulille.iut.pizzaland.dto.CommandeDto;

public class CommandeResourceTest extends JerseyTest {
	private static final Logger LOGGER = Logger.getLogger(PizzaResourceTest.class.getName());
    private CommandeDao dao;

    @Override
    protected Application configure() {
            BDDFactory.setJdbiForTests();
            return new ApiV1();
    }

    // Les méthodes setEnvUp() et tearEnvDown() serviront à terme à initialiser la base de données
    // et les DAO
    
    // https://stackoverflow.com/questions/25906976/jerseytest-and-junit-throws-nullpointerexception
    @Before
    public void setEnvUp() {
            dao = BDDFactory.buildDao(CommandeDao.class);
            dao.createCommandeAndAssociationTable();
    }

    @After
    public void tearEnvDown() throws Exception {
            dao.deleteCommandeAndAssociationTable();
    }

    @Test
    public void testGetEmptyList() {
            // La méthode target() permet de préparer une requête sur une URI.
            // La classe Response permet de traiter la réponse HTTP reçue.
            Response response = target("/commande").request().get();

            // On vérifie le code de la réponse (200 = OK)
            assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

            // On vérifie la valeur retournée (liste vide)
            // L'entité (readEntity() correspond au corps de la réponse HTTP.
            // La classe javax.ws.rs.core.GenericType<T> permet de définir le type
            // de la réponse lue quand on a un type complexe (typiquement une liste).
            List<CommandeDto> commande;
            commande = response.readEntity(new GenericType<List<CommandeDto>>(){});

            // On vérifie que la liste est bien vide 
            assertEquals(0, commande.size());
    }
    /*
    @Test
    public void testGetExistingCommande() {

            Commande commande = new Commande();
            commande.setFirstName("Elon");
            commande.setLastName("Musk");
            
            long id = dao.insert(commande);
            commande.setId(id);

            Response response = target("/commande/" + id).request().get();

            assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

            Commande result = Commande.fromDto(response.readEntity(CommandeDto.class));
            assertEquals(commande, result);
    }
    */

    @Test
    public void testGetNotExistingCommande() {
            Response response = target("/commande/125").request().get();
            assertEquals(Response.Status.NOT_FOUND.getStatusCode(),response.getStatus());
    }
    /*
    @Test
    public void testCreateCommande() {
    CommandeCreateDto commandeCreateDto = new CommandeCreateDto();
    commandeCreateDto.setFirstName("Elon");
    commandeCreateDto.setLastName("Musk");
    
    Response response = target("/commande")
            .request()
            .post(Entity.json(commandeCreateDto));

    // On vérifie le code de status à 201
    assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

    CommandeDto returnedEntity = response.readEntity(CommandeDto.class);

    // On vérifie que le champ d'entête Location correspond à
    // l'URI de la nouvelle entité
    assertEquals(target("/commande/" +
                    returnedEntity.getId()).getUri(), response.getLocation());
            
            // On vérifie que le nom correspond
    assertEquals(returnedEntity.getLastName(), commandeCreateDto.getLastName());
    }
	*/
    /*
    @Test
    public void testCreateSameCommande() {
    CommandeCreateDto commandeCreateDto = new CommandeCreateDto();
    commandeCreateDto.setFirstName("Elon");
    commandeCreateDto.setLastName("Musk");
    
    dao.insert(Commande.fromCommandeCreateDto(commandeCreateDto));

    Response response = target("/commande")
            .request()
            .post(Entity.json(commandeCreateDto));

    assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
    }
    */
    /*
    @Test
    public void testCreateCommandeWithoutName() {
    CommandeCreateDto commandeCreateDto = new CommandeCreateDto();

    Response response = target("/commande")
            .request()
            .post(Entity.json(commandeCreateDto));

    assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
    }
    */
    ////////////////////////////
    /*
    @Test
    public void testDeleteExistingCommande() {
    Commande commande = new Commande();
    commande.setFirstName("Elon");
    commande.setLastName("Musk");
    
    long id = dao.insert(commande);
    commande.setId(id);

    Response response = target("/commande/" + id).request().delete();

    assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());

    Commande result = dao.findById(id);
    assertEquals(result, null);
    }
    */

    @Test
    public void testDeleteNotExistingCommande() {
    Response response = target("/commande/125").request().delete();
    assertEquals(Response.Status.NOT_FOUND.getStatusCode(),response.getStatus());
    }
    /*
    @Test
    public void testGetCommandeName() {
    Commande commande = new Commande();
    commande.setFirstName("Elon");
    commande.setLastName("Musk");
    
    long id = dao.insert(commande);

    Response response = target("commande/" + id + "/name").request().get();

    assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

    assertEquals("Musk", response.readEntity(String.class));
    }
	*/
    @Test
    public void testGetNotExistingCommandeName() {
    Response response = target("commande/125/name").request().get();

    assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
    }
}
