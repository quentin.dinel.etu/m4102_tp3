package fr.ulille.iut.pizzaland.dao;

import java.util.List;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;

public interface CommandeDao {
	@SqlUpdate("CREATE TABLE IF NOT EXISTS commandes (id INTEGER PRIMARY KEY, firstName VARCHAR UNIQUE, lastName VARCHAR UNIQUE)")
	  void createCommandeTable();

	@SqlUpdate("CREATE TABLE IF NOT EXISTS commandePizzaAssociation(idPizza INTEGER, idCommande INTEGER, constraint pk_CommandeIA Primary key(idPizza,idCommande))")
	  void createAssociationTable();
	
	@Transaction
	default void createCommandeAndAssociationTable() {
		createAssociationTable();
		createCommandeTable();
	}
	
	@SqlUpdate("DROP TABLE IF EXISTS commandes")
	  void dropCommandeTable();
	@SqlUpdate("DROP TABLE IF EXISTS commandePizzaAssociation")
	  void dropAssociationTable();
	
	@Transaction
	default void deleteCommandeAndAssociationTable(){
	      dropAssociationTable();
	      dropCommandeTable();
	  }
	
	//first name aussi ?
	
	@SqlUpdate("INSERT INTO commandes (firstName, lastName) VALUES (:firstName, :lastName)")
	@RegisterBeanMapper(Commande.class)
	@GetGeneratedKeys
	long insert(Commande commande);

	@SqlQuery("SELECT * FROM commandes")
	@RegisterBeanMapper(Commande.class)
	List<Commande> getAll();
	
	@SqlQuery("SELECT * FROM commandes WHERE fk_pizzas = :fk_commandes")
	@RegisterBeanMapper(Commande.class)
	List<Pizza> getPizzas();
	
	@SqlQuery("SELECT * FROM commandes WHERE id = :id")
	@RegisterBeanMapper(Commande.class)
	Commande findById(long id);
	
	@SqlQuery("SELECT * FROM commandes WHERE name = :name")
	@RegisterBeanMapper(Pizza.class)
	Commande findByName(String name);
	
	@SqlUpdate("DELETE FROM commandes WHERE id = :id")
	void remove(long id);
}
