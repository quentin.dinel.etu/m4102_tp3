## Développement d'une ressource *commande*

### API et représentation des données

Nous pouvons tout d'abord réfléchir à l'API REST que nous allons offrir pour la ressource *commande*. Celle-ci devrait répondre aux URI suivantes :

| Opération | URI         | Action réalisée                               | Retour                                        |
|:----------|:------------|:----------------------------------------------|:----------------------------------------------|
| GET       | /commande | récupère l'ensemble des commandes         | 200 et un tableau de commandes              |
| GET       | /commande/{id} | récupère la commande d'identifiant id  | 200 et la commande                           |
|           |             |                                               | 404 si id est inconnu                         |
| POST      | /commande/ | création d'une commande                     | 201 et l'URI de la ressource créée + représentation |
|           |             |                                               | 400 si les informations ne sont pas correctes |
|           |             |                                               | 409 si la commande existe déjà (même nom)    |
| DELETE    | /commande/{id} | destruction de la commande d'identifiant id | 202 si l'opération à réussi                   |
|           |             |                                               | 404 si id est inconnu                         |

Une commande comporte un identifiant, le prénom et le nom de celui qui commande et aussi la liste des pizzas. Sa
représentation JSON prendra donc la forme suivante :

    {
	  "id": 1,
	  "firstName": "Elon",
	  "lastName": "Musk",
	  "pizzas": [{"id": 1, "name": "forestiere"},{"id": 2, "name": "chorizo"}]
	}
	
Lors de la création, l'identifiant n'est pas connu car il sera fourni
par la base de données. Voici sa représentation JSON lors d'un GET :

    {"id":2,"firstName":"Elon","lastName":"Musk"}
    
    
