## Développement d'une ressource *pizzas*

### API et représentation des données

Nous pouvons tout d'abord réfléchir à l'API REST que nous allons offrir pour la ressource *pizzas*. Celle-ci devrait répondre aux URI suivantes :

| Opération | URI         | Action réalisée                               | Retour                                        |
|:----------|:------------|:----------------------------------------------|:----------------------------------------------|
| GET       | /pizzas | récupère l'ensemble des pizzas         | 200 et un tableau de pizzas              |
| GET       | /pizzas/{id} | récupère la pizza d'identifiant id  | 200 et la pizza                           |
|           |             |                                               | 404 si id est inconnu                         |
| POST      | /pizzas/ | création d'une pizza                     | 201 et l'URI de la ressource créée + représentation |
|           |             |                                               | 400 si les informations ne sont pas correctes |
|           |             |                                               | 409 si la pizza existe déjà (même nom)    |
| DELETE    | /pizzas/{id} | destruction de la pizza d'identifiant id | 202 si l'opération à réussi                   |
|           |             |                                               | 404 si id est inconnu                         |

Une pizza comporte un identifiant, un nom et une liste d'ingrédients. Sa
représentation JSON prendra donc la forme suivante :

    {
	  "id": 1,
	  "name": "forestiere",
	  "ingredients": [{"id": 1, "name": "champignons"},{"id": 2, "name": "jambon"}]
	}
	
Lors de la création, l'identifiant n'est pas connu car il sera fourni
par la base de données. Voici sa représentation JSON lors d'un GET :

    {"id":2,"name":"jambon"}
    
    