package fr.ulille.iut.pizzaland.dto;

import java.util.List;

import fr.ulille.iut.pizzaland.beans.Pizza;

public class CommandeDto {
	private long id;
	private String nom;
	private String prenom;
	private List<Pizza> pizzas;

	public CommandeDto() {}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return prenom;
	}

	public void setFirstName(String name) {
		this.prenom = name;
	}
	
	public String getLastName() {
		return nom;
	}

	public void setLastName(String name) {
		this.nom = name;
	}
	
	public void setPizzas(List<Pizza> pizzas) {
		this.pizzas = pizzas;
	}
	
	public List<Pizza> getPizzas() {
		return pizzas;
	}
}
