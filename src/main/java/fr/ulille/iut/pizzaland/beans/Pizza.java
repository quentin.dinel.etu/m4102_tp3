package fr.ulille.iut.pizzaland.beans;

import java.util.List;

import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

public class Pizza {
	private long id;
	private String name;
	private List<Ingredient> ingredients;
	
	public Pizza() {
	}

	public Pizza(long id, String name, List<Ingredient> ingredients) {
		this.id = id;
		this.name = name;
		this.ingredients = ingredients;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}
	
	public List<Ingredient> getIngredients(){
		return ingredients;
	}
	
	public void addIngredient(Ingredient ingredient) {
		ingredients.add(ingredient);
	}

	public static PizzaDto toDto(Pizza i) {
		PizzaDto dto = new PizzaDto();
		dto.setId(i.getId());
		dto.setName(i.getName());
		dto.setIngredients(i.getIngredients());
		return dto;
	}

	public static Pizza fromDto(PizzaDto dto) {
		Pizza Pizza = new Pizza();
		Pizza.setId(dto.getId());
		Pizza.setName(dto.getName());
		Pizza.setIngredients(dto.getIngredients());
		return Pizza;
	}
	//CREATION DE PIZZA
	public static PizzaCreateDto toCreateDto(Pizza Pizza) {
	    PizzaCreateDto dto = new PizzaCreateDto();
	    dto.setName(Pizza.getName());
	    dto.setIngredients(Pizza.getIngredients());
	    return dto;
	}

	public static Pizza fromPizzaCreateDto(PizzaCreateDto dto) {
	    Pizza Pizza = new Pizza();
	    Pizza.setName(dto.getName());
	    Pizza.setIngredients(dto.getIngredients());
	    return Pizza;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pizza other = (Pizza) obj;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return name;
	}
	
	public String toStringIngredients() {
		String res = "";
		for(Ingredient i : ingredients) {
			res+=i.toString();
		}
		return res;
	}
}
