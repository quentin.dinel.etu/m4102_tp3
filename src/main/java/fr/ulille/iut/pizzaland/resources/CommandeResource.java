package fr.ulille.iut.pizzaland.resources;

import java.net.URI;
import java.util.List;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import fr.ulille.iut.pizzaland.dto.CommandeCreateDto;
import fr.ulille.iut.pizzaland.dto.CommandeDto;
import javax.ws.rs.PathParam;
import javax.ws.rs.WebApplicationException;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.dao.CommandeDao;

@Path("/commande")
public class CommandeResource {
	
	private static final Logger LOGGER = Logger.getLogger(CommandeResource.class.getName());
	private CommandeDao commandes;
	@Context
	public UriInfo uriInfo;

	public CommandeResource() {
		commandes = BDDFactory.buildDao(CommandeDao.class);
		commandes.createCommandeAndAssociationTable();
	}

	@GET
    public List<CommandeDto> getAll() {
        LOGGER.info("CommandeResource:getAll");
        List<CommandeDto> l = commandes.getAll().stream().map(Commande::toDto).collect(Collectors.toList());
        return l;
    }
    
    @GET
    @Path("{id}")
    public CommandeDto getOneCommande(@PathParam("id") long id) {
    	LOGGER.info("getOneCommande(" + id + ")");
        try {
            Commande commande = commandes.findById(id);
    		return Commande.toDto(commande);
        }
        catch ( Exception e ) {
    		// Cette exception générera une réponse avec une erreur 404
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
    }

    @POST
    public Response createCommande(CommandeCreateDto commandeCreateDto) {
    	Commande existing = commandes.findByName(commandeCreateDto.getLastName());
    	if ( existing != null ) {
    		throw new WebApplicationException(Response.Status.CONFLICT);
    	}

    	try {
    		Commande commande = Commande.fromCommandeCreateDto(commandeCreateDto);
    		long id = commandes.insert(commande);
    		commande.setId(id);
    		CommandeDto CommandeDto = Commande.toDto(commande);

    		URI uri = uriInfo.getAbsolutePathBuilder().path("" + id).build();

    		return Response.created(uri).entity(CommandeDto).build();
    	}
    	catch ( Exception e ) {
    		e.printStackTrace();
    		throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
    	}
    }
    
    @DELETE
    @Path("{id}")
    public Response deleteCommande(@PathParam("id") long id) {
    	if ( commandes.findById(id) == null ) {
    		throw new WebApplicationException(Response.Status.NOT_FOUND);
    	}

        commandes.remove(id);

        return Response.status(Response.Status.ACCEPTED).build();
    }
    
    @GET
    @Path("{id}/name")
    public String getCommandeName(@PathParam("id") long id) {
        Commande Commande = commandes.findById(id);
    	if ( Commande == null ) {
    		throw new WebApplicationException(Response.Status.NOT_FOUND);
    	}
            
    	return Commande.getFirstName() + " " + Commande.getLastName();
    }
    
}